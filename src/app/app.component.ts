import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pantalla-clinicaUniversidad-turnos';


  listaRecepcion = [
    {
      numero:'A 092',
      modulo:'03'
    },
    {
      numero:'A 091',
      modulo:'01'
    },
  ];


  listaConsulta = [
    {
      nombre:'M.VIAL',
      medico:'C.ORTIZ',
      box:'06'
    },
    {
      nombre:'M.GARCIA',
      medico:'G.CARCURO',
      box:'08'
    },
    {
      nombre:'A.MORGAN',
      medico:'J.FLEIDERMAN',
      box:'03'
    },
    {
      nombre:'M.BUZETA',
      medico:'E.CARRASCO',
      box:'16'
    },
  ];
}
